# frozen_string_literal: true

namespace :data do
  namespace :users do
    desc 'Creates User Own Groups'
    task assign_orphan_groups: :environment do
      assigned = 0
      incidents = Incident.where group: nil
      Incident.transaction do
        puts "#{incidents.size} orphan incidents found."
        incidents.all.each do |incident|
          group = incident.user.own_group
          puts "Assigning group #{group.id} to incident #{incident.id}"
          incident.group = group
          incident.save
          assigned += 1
        end
      end
      puts "Done! Re-assigned #{assigned} incidents."
    end
  end
end
