# frozen_string_literal: true

namespace :data do
  namespace :users do
    desc 'Creates User Own Groups'
    task create_own_groups: :environment do
      created = 0
      User.transaction do |_t|
        User.all.each do |user|
          puts "Creating group for #{user.username}..."
          user.own_group
          created += 1
        end
      end
      puts "Done! Created #{created} groups."
    end
  end
end
