@Server = http://localhost:3000
# @Server = https://make-sec.org
@ContentType = application/json
@JWTtoken = {{Login.response.body.$.token}}

# @name Signup
POST {{Server}}/signup HTTP/1.1
Content-Type: {{ContentType}}

{
    "username": "gomezhyuuga",
    "email": "gomezhyuuga@gmail.com",
    "password": "foobar1234",
    "password_confirmation": "foobar1234"
}

###

# @name Login
POST {{Server}}/login HTTP/1.1
Content-Type: {{ContentType}}

{
    "user": {
        "username": "gomezhyuuga",
        "password": "foobar1234"
    }
}

###

# @name ResetPassword
POST {{Server}}/password HTTP/1.1
Content-Type: {{ContentType}}

{
	"user": {
		"email": "gomezhyuuga@gmail.com"
	}
}

###

# @name UserIncidentsLIST
GET {{Server}}/user/incidents HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

###

# @name UserIncidentsGET
GET {{Server}}/user/incidents/5 HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

###

# @name UserIncidentsCREATE
POST {{Server}}/user/incidents HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

{
	"title": "Example Test",
	"category": "Asalto",
	"description": "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec ullamcorper nulla non metus auctor fringilla. Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper.",
	"date": "2010-10-11",
	"time": "10:10",
	"location_latitude": 19.432241,
	"location_longitude": -99.177254,
	"location_address": "Av Lago de Guadalupe KM 3.5",
	"location_state": "Estado de México",
	"location_city": "Atizapán de Zaragoza",
	"location_country": "Mexico",
	"location_zip_code": "52926",
	"group_id": 4
}
###

# @name UserIncidentComplementsCREATE
POST {{Server}}/user/incidents/5/complements HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

{
	"incident": 5,
	"title": "Example Test",
	"category": "Asalto",
	"description": "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec ullamcorper nulla non metus auctor fringilla. Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper.",
	"date": "2010-10-11",
	"time": "14:20",
	"location_latitude": 19.432241,
	"location_longitude": -99.177254,
	"location_address": "Av Lago de Guadalupe KM 3.5",
	"location_state": "Estado de México",
	"location_city": "Atizapán de Zaragoza",
	"location_country": "Mexico",
	"location_zip_code": "52926",
	"group_id": 4
}
###

###
# @name UserGroupsLIST
GET {{Server}}/user/groups HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

###
# @name UserGroupsCREATE
POST {{Server}}/user/groups HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}

{
	"name": "TEC CEM rmroman",
	"members": ["gomezhyuuga", "rmroman", "ltrejo"]
}

###

# @name UserGroupsIncidentsLIST
GET {{Server}}/user/groups/4/incidents HTTP/1.1
Content-Type: {{ContentType}}
Authorization: Bearer {{JWTtoken}}
