FactoryBot.define do
  factory :incident do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    date { Faker::Date.backward }
    time { Faker::Time.backward }
    event_type { :regular_event }

    location_address { Faker::Address.street_address }
    location_state { Faker::Address.street_address }
    location_city { Faker::Address.city }
    location_zip_code { Faker::Address.zip_code }
    location_country { Faker::Address.country }

    incident_category
    user { create :user, :confirmed }

    trait :regular do
      event_type { :regular_event }
    end

    trait :panic do
      event_type { :panic_event }
      date { Faker::Date.backward }
      time { Faker::Time.backward }
      location_latitude { Faker::Address.latitude }
      location_longitude { Faker::Address.longitude }
    end
  end
end
