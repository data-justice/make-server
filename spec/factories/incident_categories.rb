FactoryBot.define do
  factory :incident_category do
    description { Faker::Lorem.unique.word }
  end
end
