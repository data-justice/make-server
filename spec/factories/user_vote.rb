FactoryBot.define do
  factory :poll_choice do
    title { Faker::Lorem.words 2 }
    poll
  end

  factory :poll do
    group { create :group }
    user { create :user, :confirmed }
    poll_choices { build_list :poll_choice, 4 }

    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
  end
end
