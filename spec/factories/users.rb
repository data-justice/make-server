FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    username { Faker::Internet.username }
    password { Faker::Internet.password }
    password_confirmation { password }

    trait :confirmed do
      after(:create) do |user, evaluator|
        user.confirm
      end
    end

    factory :user_with_incidents do
      transient do
        incidents_count { 8 }
      end
       after(:create) do |user, evaluator|
         user.confirm
         create_list(:incident, evaluator.incidents_count, user: user)
      end
    end
  end
end
