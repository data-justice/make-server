FactoryBot.define do
  factory :security_plan do
    group
    missing_person { create :user }
  end
end
