FactoryBot.define do
  factory :group do
    name { Faker::Lorem.word }
    governance_model { 'democratic' }


    transient do
      members_count { 6 }
    end

    factory :group_with_members do
      user
      members { create_list(:user, members_count).collect(&:username) }
    end
  end
end
