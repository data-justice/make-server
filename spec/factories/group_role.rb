FactoryBot.define do
  factory :group_role do
    name { Faker::Lorem.unique.word }
  end
end
