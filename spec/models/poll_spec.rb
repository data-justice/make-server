require 'rails_helper'

RSpec.describe Poll, type: :model do
  it { should validate_presence_of :user }
  it { should validate_presence_of :group }
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :poll_choices }

  it { should have_many :poll_choices }
  it { should belong_to :group }
  it { should belong_to :user }
end
