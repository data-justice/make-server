require 'rails_helper'

RSpec.describe Group, type: :model do
  it { should belong_to(:active_security_plan).optional }
  it { should belong_to :user }
  it { should belong_to(:centralized_person).optional }
  it { should have_many :users }
  it { should have_many :group_memberships }
  it { should have_many :security_plans }
  it { should validate_presence_of :name }
  it { should validate_presence_of :governance_model }

  # Stable logic
  it { should respond_to :stable_security_plan }

  describe "#stable_security_plan" do
    subject { create :group_with_members, members_count: 3 }

    it 'returns a security plan associated to a stable group' do
      sec_plan = subject.stable_security_plan

      expect(sec_plan).not_to be nil
      expect(sec_plan).to be_a_kind_of SecurityPlan
      expect(sec_plan.missing_person).to be nil
      expect(sec_plan).to be_stable
    end
  end

  #describe "Stable Logic" do
    #describe "#set_stable" do
    #  it "removes any active security plan" do
    #    expect(subject.stable).to be true
    #    expect(subject.active_security_plan).to be_nil
    #    expect(subject.governance_details).not_to be_nil
    #  end
    #end
    #describe "Update votes" do
    #  #subject create
    #end
  #end
end