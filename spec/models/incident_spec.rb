# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Incident, type: :model do
  let!(:event_types) { %i[regular_event panic_event] }
  # Relations
  it { should belong_to(:incident_category) }
  it { should belong_to(:user) }
  it { should belong_to(:group).optional }

  # Validations
  it { should validate_presence_of :user }

  it { should validate_presence_of :title }
  it { should validate_presence_of :date }
  it { should validate_presence_of :time }
  it { should validate_presence_of :event_type }
  it { should validate_presence_of :incident_category }
  it { should define_enum_for(:event_type).with_values(event_types) }

  it { should validate_presence_of :description }

  context 'initializer' do
    it 'should be initialized as regular' do
      incident = build :incident
      expect(incident).to be_regular_event
    end
  end

  describe 'hooks' do
    describe 'create' do
      subject { build :incident }
      it 'assigns default group to user own group if nothing specified' do
        subject.group = nil
        subject.group_id = nil

        subject.save!

        expect(subject.group).to eq subject.user.own_group
      end
    end
  end

  context 'regular incident' do
    subject { build(:incident, :regular) }

    it { should validate_presence_of :location_address }
    it { should validate_presence_of :location_state }
    it { should validate_presence_of :location_city }
    it { should validate_presence_of :location_zip_code }
    it { should validate_presence_of :location_country }
  end

  context 'panic event' do
    subject { build(:incident, :panic) }
    it { should_not validate_presence_of :location_address }
    it { should_not validate_presence_of :location_state }
    it { should_not validate_presence_of :location_city }
    it { should_not validate_presence_of :location_zip_code }
    it { should_not validate_presence_of :location_country }
  end
end
