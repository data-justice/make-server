require 'rails_helper'

RSpec.describe PollChoice, type: :model do
  it { should validate_presence_of :title }
  it { should validate_presence_of :poll }

  it { should belong_to :poll }
end
