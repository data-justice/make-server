require 'rails_helper'

RSpec.describe GroupMembership, type: :model do
  it { should belong_to :user }
  it { should belong_to :group }

  it { should validate_numericality_of(:vote_weight).is_greater_than_or_equal_to(0) }
end