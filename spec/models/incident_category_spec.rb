require 'rails_helper'

RSpec.describe IncidentCategory, type: :model do
  subject { FactoryBot.create(:incident_category) }
  # Relations
  it { should have_many(:incidents).dependent(:nullify) }

  it { should validate_presence_of :description }
  it { should validate_uniqueness_of(:description).case_insensitive }
end
