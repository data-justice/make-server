require 'rails_helper'

RSpec.describe UserGroupRole, type: :model  do
  it { should belong_to :user }
  it { should belong_to :security_plan }
  it { should belong_to :group_role }
  it { should validate_presence_of :group_role }
  it { should validate_presence_of :user }
  it { should validate_presence_of :security_plan }
end