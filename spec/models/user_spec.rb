# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { should have_many :incidents }
    it { should have_many :group_memberships }
    it { should have_many :user_votes }
  end

  describe '#own_group' do
    subject { create :user }
    it 'returns always a valid group with isOwn = true' do
      group = subject.own_group
      expect(group).not_to be_nil
      expect(group.isOwn).to be true
      expect(group.user).to eq subject
    end
  end

  describe 'Hooks' do
    describe 'after_create' do
      subject { build(:user) }
      it 'creates default group with isOwn = true' do
        user = subject
        user.save!
        group = Group.find_by(user: user, isOwn: true)
        expect(group).not_to be_nil
      end
    end
  end
end
