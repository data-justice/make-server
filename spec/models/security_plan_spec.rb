require 'rails_helper'

RSpec.describe SecurityPlan, type: :model do
  it { should validate_presence_of :group }

  it { should respond_to :stable? }
  it { should respond_to :votes }
  it { should respond_to :votes_details }

  context "Scopes" do
    describe "#missing_person_in_group" do
      let!(:group) { create :group_with_members, members_count: 3 }
      let(:security_plan) { group.security_plans.first }
      let(:missing_person) { group.users[0] }

      it 'returns the Security Plan given a username' do
        result = SecurityPlan.missing_person_in_group(missing_person.username, group.id).first
        expect(result.missing_person.id).to eq(missing_person.id)
      end
    end
  end

  describe "#votes" do
    let!(:group) { create :group_with_members, members_count: 3 }
    let(:members) { group.members }
    let(:voters) { group.users.reject { |u| u.id == group.user.id } }
    let(:security_plan) { group.security_plans.first }

    before do
      votes = [:democratic, :democratic, :centralized]
      UserGovernanceVote.create!( security_plan: security_plan, user: voters[0], governance_model: votes[0])
      UserGovernanceVote.create!( security_plan: security_plan, user: voters[1], governance_model: votes[1])
      UserGovernanceVote.create!( security_plan: security_plan, user: voters[2], governance_model: votes[2])
    end

    it 'contains votes summary' do
      expect(security_plan.votes.keys).to contain_exactly(:votes,
                                                          :votes_details,
                                                          :winner,
                                                          :tie)
    end

    it 'returns a correct count of votes' do
      result = security_plan.votes
      expect(result[:votes]).to eq({centralized: 1, democratic: 2, weighted: 0})
      expect(result[:tie]).to be_falsey
      expect(result[:winner]).to eq :democratic
    end
    it 'returns the details of the votes' do
      result = security_plan.votes
      expect(result[:votes_details]).to eq({ democratic: [voters[0].username, voters[1].username],
                                          centralized: [voters[2].username],
                                          weighted: []})
    end
  end
end
