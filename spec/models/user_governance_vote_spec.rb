# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserGovernanceVote, type: :model do
  it { should validate_presence_of :user }
  it { should validate_presence_of :security_plan }
  it { should validate_presence_of :governance_model }
end
