require 'rails_helper'

RSpec.describe UserVote, type: :model do
  it { should belong_to :user }
  it { should belong_to :poll }
  it { should belong_to :poll_choice }

  it { should validate_presence_of :user }
  it { should validate_presence_of :poll }
  it { should validate_presence_of :poll_choice }
end
