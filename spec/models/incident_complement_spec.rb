require 'rails_helper'

RSpec.describe IncidentComplement, type: :model do
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :date }
  it { should validate_presence_of :time }
  it { should validate_presence_of :incident }
end
