# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'USER - Groups', type: :request do
  context 'Authenticated' do
    let!(:user) { create :user, :confirmed }
    let!(:other_users) { create_list :user, 5, :confirmed }
    let!(:other_users_usernames) { other_users.collect(&:username) }
    let(:headers) do
      Devise::JWT::TestHelpers.auth_headers({
                                              'Accept' => 'application/json',
                                              'Content-Type' => 'application/json'
                                            },
                                            user)
    end

    describe 'PUT /user/groups/:id/activate_security_plan' do
      let!(:group) { create :group_with_members, members_count: 3 }
      let(:members) { group.members }
      let(:voters) { group.users.reject { |u| u.id == group.user.id } }
      let(:security_plan) { group.security_plans.first }
      let!(:missing_person) { security_plan.missing_person.username }
      let!(:valid_params) { { missing_person: missing_person }.to_json }

      before do
        votes = %i[democratic democratic centralized]
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[0], governance_model: votes[0])
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[1], governance_model: votes[1])
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[2], governance_model: votes[2])
        # Winner is democratic for first security plan
      end

      describe 'with valid params' do
        before { put "/user/groups/#{group.id}/activate_security_plan", headers: headers, params: valid_params }

        it 'returns 200 status code' do
          expect(response).to have_http_status(200)
        end

        it 'includes the information about the changes' do
          expect(json.keys).to contain_exactly('active_security_plan', 'group')
        end

        it 'returns the correct active security plan' do
          expect(json).to include_json(active_security_plan: {
                                         id: security_plan.id,
                                         missing_person: missing_person,
                                         governance_details: {
                                           winner: 'democratic'
                                         }
                                       }, group: {
                                         active_security_plan_id: security_plan.id,
                                         governance_model: 'democratic'
                                       })
        end
        # expect(json['message']).to eq("Se ha establecido el plan de seguridad con #{missing_person} como persona desaparecida. El modelo de gobernanza seleccionado fue: Democrático")
      end
      # describe 'with invalid params' do
      #   before { put "/user/groups/#{group.id}/activate_security_plan", headers: headers, params: {} }
      #   it 'returns invalid missing person' do
      #
      #   end
      # end
    end

    describe 'PATCH /user/groups/:id' do
      let!(:group) { create :group_with_members, members_count: 4 }
      let!(:new_members) { [group.users[1], group.users[2]] }
      let!(:new_members_usernames) { new_members.collect(&:username) }
      before { patch "/user/groups/#{group.id}", headers: headers, params: { name: 'My new name' }.to_json }

      it 'updates correctly the name' do
        expect(json['name']).to eq 'My new name'
      end

      describe 'updating members' do
        before { patch "/user/groups/#{group.id}", headers: headers, params: { members: new_members.collect(&:username) }.to_json }
        it 'updates correctly the members' do
          expect(json['members'].size).to eq 3
          memberships = GroupMembership.where(group_id: json['id']).count
          expect(memberships).to eq 3
          usernames = json['members'].map { |x| x['username'] }
          expect(usernames).to contain_exactly(*new_members_usernames, user.username)
        end
      end

      #
      # it 'sets the security plan' do
      #
      # end
    end

    describe 'GET /user/groups' do
      let!(:user_1) { create :user, :confirmed }
      let!(:user_2) { create :user, :confirmed }
      let!(:group) { create :group, user: user, members: [user.username, user_1.username, user_2.username] }
      before { get '/user/groups', headers: headers }
      it 'returns 200 status code' do
        expect(response).to have_http_status(200)
      end
      it 'gets the groups created by own user' do
        expect(json.size).to eq (1 + 1) # +1 because of the default user isOwn group
        expect(json).to include_json([{ name: a_kind_of(String), creator: user.username }])
      end

      context 'as a group member' do
        describe 'GET /user/groups' do
          before do
            headers_2 = Devise::JWT::TestHelpers.auth_headers({ 'Accept' => 'application/json', 'Content-Type' => 'application/json' }, user_1)

            get '/user/groups', headers: headers_2
          end
          it 'gets the groups where user is invited' do
            expect(json.size).to eq (1 + 1) # + 1 because of the isOwn group
            expect(json).to include_json([{ name: a_kind_of(String) }])
          end
        end
      end
    end

    describe 'POST /user/groups' do
      let!(:mock_group) { build :group }
      let(:members) { other_users.collect &:username }
      let(:members_ids) { other_users.collect &:id }
      let(:valid_params) { { name: mock_group.name, members: members }.to_json }
      before do
        post '/user/groups', params: valid_params, headers: headers
      end

      it 'sets group creator' do
        expect(json['creator']).to eq user.username
      end

      it 'sets default governance to democratic' do
        expect(json['governance_model']).to eq 'democratic'
      end
      it 'sets default security plan to STABLE' do
        group_id    = json['id']
        group       = Group.find(group_id)
        stable_plan = group.stable_security_plan
        sec_plan = json['active_security_plan']
        expect(sec_plan).not_to be_nil
        expect(sec_plan['id']).to eq stable_plan.id
      end

      it 'creates memberships' do
        group_id    = json['id']
        group       = Group.find(group_id)
        memberships = group.group_memberships
        # Creates members + 1 memberships
        expect(memberships.size).to eq(other_users.size + 1)
      end

      it 'creates security plans' do
        group_id       = json['id']
        group          = Group.find group_id
        security_plans = SecurityPlan.where group_id: group.id

        # + 1 (due to self member missing) + 1 (stable security plan)
        expect(security_plans.size).to eq members_ids.size + 1 + 1

        missing_persons = security_plans.each.collect &:missing_person_id
        # nil due to stable security plan
        expect(missing_persons).to contain_exactly(*members_ids, user.id, nil)
      end

      it 'creates stable security plan' do
        group_id = json['id']
        group = Group.find group_id
        security_plan = SecurityPlan.find_by! group_id: group.id, missing_person: nil
        expect(security_plan).not_to be nil
        expect(security_plan.missing_person).to be nil
      end

      it 'sets group invitations as pending' do
        group_id    = json['id']
        group       = Group.find(group_id)
        memberships = group.group_memberships
        # Creates members + 1 memberships
        statuses = memberships.collect &:pending
        expect(statuses.all?(true)).to be true
      end
    end
  end
end
