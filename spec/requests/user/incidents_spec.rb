# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'USER - Incidents', type: :request do
  describe 'POST /user/incidents' do
    let!(:incident) { build :incident, user: nil }
    let!(:incident_category) { create :incident_category }
    context 'authenticated' do
      let(:user) { create :user, :confirmed }
      let(:headers) do
        Devise::JWT::TestHelpers.auth_headers({
                                                'Accept' => 'application/json',
                                                'Content-Type' => 'application/json'
                                              },
                                              user)
      end
      let(:valid_params) do
        valid = incident.attributes
        valid['category'] = incident_category.description
        valid.to_json
      end

      context 'with VALID params' do
        before { post '/user/incidents', params: valid_params, headers: headers }

        it 'returns status code 201' do
          expect(response).to have_http_status 201
        end

        it 'creates the incident as regular type' do
          expect(json).not_to be_empty
          expect(json['title']).not_to be_empty
          expect(json['id']).not_to be_nil
          expect(json['category']).to eq incident_category.description
          expect(json['category_id']).not_to be_nil
          expect(json['user']).to eq user.username
          expect(json['regular_event']).to be true
          expect(json['panic_event']).to be false
        end
        # context 'with INVALID params' do
        # end

        it 'assigns as default group if no specified' do
          expect(json['group_id']).not_to be_nil
          expect(json['group']).not_to be_nil
          expect(json['group_id']).to eq user.own_group.id
        end
      end
    end

    context 'no authenticated' do
    end
  end

  describe 'GET /user/incidents' do
    let!(:user) { create :user_with_incidents }
    let(:incidents) { user.incidents }

    context 'no authenticated' do
      before { get '/user/incidents' }

      it 'returns status code 401' do
        expect(response).to have_http_status 401
      end
    end

    context 'authenticated' do
      let(:headers) { Devise::JWT::TestHelpers.auth_headers({ 'Accept' => 'application/json', 'Content-Type' => 'application/json' }, user) }

      before do
        get '/user/incidents/', headers: headers
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns list of user incidents' do
        expect(json).not_to be_empty
        expect(json.size).to eq incidents.size

        users = json.collect { |i| i['user'] }
        expect(users.uniq).to eq [user.username]
      end

      it 'returns list sorted by date' do
        format = '%Y-%m-%d %H:%M'
        dates = json.collect do |incident|
          Date.strptime "#{incident['date']} #{incident['time']}", format
        end
        expect(dates).to eq dates.sort.reverse
      end
    end
  end
end
