# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User - Security Plans' do
  context 'Authenticated' do
    let!(:user) { create :user, :confirmed }
    let!(:other_users) { create_list :user, 4, :confirmed }
    let!(:group_members) { other_users.collect(&:username) }
    let!(:group_roles) { create_list :group_role, 7 }
    let(:headers) do
      Devise::JWT::TestHelpers.auth_headers({
                                                'Accept'       => 'application/json',
                                                'Content-Type' => 'application/json'
                                            },
                                            user)
    end

    let(:valid_group_params) { {name: 'Testing Group', members: group_members} }

    before do
      post '/user/groups/', headers: headers, params: valid_group_params.to_json
      @group = Group.find json['id'].to_i
    end

    describe 'POST /user/groups/:group_id/security_plans' do
      let!(:valid_params) do
        {group:            @group.id,
         missing_person:   other_users[0].username,
         roles:            group_roles.sample(3).collect(&:id),
         governance_model: 'democratic'}
      end


      before do
        post "/user/groups/#{@group.id}/security_plans", headers: headers, params: valid_params.to_json
      end

      describe 'already existing plan' do
        before do
          post "/user/groups/#{@group.id}/security_plans", headers: headers, params: valid_params.to_json
        end

        it 'returns status code 422' do
          expect(response).to have_http_status 422
        end
        it 'returns message indicating that plan already exists' do
          expect(json['message']).to eq("Ya existe un plan de seguridad del usuario actual (#{user.username}) para la persona faltante (#{valid_params[:missing_person]}).")
          expect(json['existing']).to_not be_nil
        end
      end

      it 'assigns the roles to the current user' do
        expect(json.keys).to contain_exactly('group',
                                             'group_name',
                                             'missing_person',
                                             'security_plan_id',
                                             'governance_selected',
                                             'governance_vote_id',
                                             'roles')
        expect(json['group']).to eq @group.id
        expect(json['group_name']).to eq @group.name
        expect(json['missing_person']).to eq valid_params[:missing_person]

        roles = json['roles']
        expect(roles.size).to eq valid_params[:roles].size

        roles_name = roles.collect { |role| role['name'] }
        roles_ids  = roles.collect { |role| role['id'] }

        expect(roles_ids).to contain_exactly *valid_params[:roles]
        expect(roles_name.any?(&:nil?)).to eq false
      end

      it 'emits vote for the governance model for the missing person' do
        expect(json).to include_json({governance_selected: valid_params[:governance_model],
                                      governance_vote_id:  a_kind_of(Numeric)})

        security_plan   = SecurityPlan.find json['security_plan_id']
        governance_vote = UserGovernanceVote.find json['governance_vote_id']
        expect(governance_vote.user_id).to eq user.id
        expect(governance_vote.security_plan_id).to eq security_plan.id
        expect(governance_vote.governance_model).to eq valid_params[:governance_model]
      end
    end

    describe 'GET /user/groups/:group_id/security_plans/template' do
      before do
        get "/user/groups/#{@group.id}/security_plans/template", headers: headers
      end
      it 'includes all the members' do
        expect(json['available_roles']).not_to be_empty
        expect(json['available_roles'].size).to eq group_roles.size

        expect(json['group']).to eq @group.name
        expect(json['group_id']).to eq @group.id

        expect(json['available_governance_models']).not_to be_empty
        expect(json['available_governance_models']).to contain_exactly('democratic', 'weighted', 'centralized')

        expect(json['available_members']).not_to be_empty
        expect(json['available_members'].size).to eq group_members.size
        expect(json['available_members']).to contain_exactly(*group_members)
        expect(json['available_members']).to_not include(user.username)
      end
    end

    context "Preset security plans" do
      let!(:group) { create :group_with_members, members_count: 3 }
      let(:members) { group.members }
      let(:voters) { group.users.reject { |u| u.id == group.user.id } }
      let(:security_plan) { group.security_plans.first }

      before do
        votes = [:democratic, :democratic, :centralized]
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[0], governance_model: votes[0])
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[1], governance_model: votes[1])
        UserGovernanceVote.create!(security_plan: security_plan, user: voters[2], governance_model: votes[2])
      end

      describe "GET /user/groups/:group_id/security_plans" do
        before do
          get "/user/groups/#{group.id}/security_plans", headers: headers
        end

        it 'contains the correct governance_details' do
          expect(json).to include_json([{missing_person:     a_kind_of(String),
                                         members_roles:      a_kind_of(Hash),
                                         governance_details: a_kind_of(Hash),
                                        }])
          first_sec_plan = json[0]
          expect(first_sec_plan['governance_details']).to include_json({
                                                                           "votes":         {
                                                                               "democratic":  2,
                                                                               "weighted":    0,
                                                                               "centralized": 1
                                                                           },
                                                                           "tie":           false,
                                                                           "winner":        "democratic",
                                                                           "votes_details": {
                                                                               "democratic":  [
                                                                                                  voters[0].username,
                                                                                                  voters[1].username
                                                                                              ],
                                                                               "weighted":    [],
                                                                               "centralized": [voters[2].username]
                                                                           }
                                                                       })
        end
      end
    end
  end
end
