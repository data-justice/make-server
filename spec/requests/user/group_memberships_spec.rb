# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'USER - GroupMemberships', type: :request do
  context 'Authenticated' do
    let!(:user) { create :user, :confirmed }
    let!(:other_users) { create_list :user, 5, :confirmed }
    let(:headers) do
      Devise::JWT::TestHelpers.auth_headers({
                                              'Accept' => 'application/json',
                                              'Content-Type' => 'application/json'
                                            },
                                            user)
    end

    describe 'GET /user/group_memberships' do
      before { get '/user/group_memberships', headers: headers }

      it 'returns status code 200' do
        expect(response).to have_http_status 200
      end

      it 'returns memberships grouped by status' do
        expect(json).to include('active', 'pending')
      end

      it 'returns empty array for non existing memberships in group' do
        expect(json['pending']).to eq []
      end

      context 'with memberships' do
        let!(:group) { create :group, user: user }
      end
    end
  end
end
