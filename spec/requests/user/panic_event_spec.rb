require 'rails_helper'

RSpec.describe 'USER - Incidents', type: :request do
  describe 'POST /user/panic_event' do
    let!(:panic_event) { build :incident, :panic, user: nil, title: nil, description: nil }
    let!(:category) {create :incident_category}
    context 'authenticated' do
      let(:user) { create :user, :confirmed }
      let(:headers) do
        Devise::JWT::TestHelpers.auth_headers({Accept: 'application/json', 'Content-Type' => 'application/json'}, user)
      end
      let(:valid_params) do
        valid = panic_event.attributes
        valid['category'] = category.description
        valid.to_json
      end

      context 'with VALID params' do
        before { post '/user/panic_event', params: valid_params, headers: headers }

        it 'returns status code 201' do
          expect(response).to have_http_status 201
        end

        it 'creates the incident with proper attributes' do
          expect(json).not_to be_empty
          expect(json['id']).not_to be_nil
          expect(json['user']).to eq user.username
          expect(json['title']).not_to be_empty
          expect(json['title']).to match(/Evento de Pánico creado/)
          expect(json['description']).to match(/Evento creado a través del botón de pánico./)
          expect(json['category']).to eq category.description
          expect(json['category_id']).not_to be_nil
        end

        it 'creates the incident as panic_event type' do
          expect(json['regular_event']).to be false
          expect(json['panic_event']).to be true
        end
        #context 'with INVALID params' do
        #end
      end
    end

    context 'no authenticated' do
    end
  end
end

