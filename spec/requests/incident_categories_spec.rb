require 'rails_helper'

RSpec.describe 'Incident Categories API', type: :request do
  # Initialize test data
  let!(:categories) { create_list(:incident_category, 10) }
  let(:category_id) { categories.first.id }

  describe 'GET /incident_categories' do
    before { get '/incident_categories' }

    it 'returns seeded categories' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST /incident_categories' do
    let(:valid_attributes) { { description: "My new category" } }

    context 'when VALID' do
      before {  post '/incident_categories', params: valid_attributes }

      it 'creates a new category' do
        expect(json['description']).to eq('My new category')
      end
    end

    context 'when INVALID' do
      before {  post '/incident_categories', params: {} } 

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns validation message' do
        expect(response.body).to match(/La validación falló: Descripción no puede estar en blanco/)
      end

      describe 'repeated category name' do
        let!(:category) { create(:incident_category) }

        before { post '/incident_categories', params: { description: category.description } }

        it 'should validate repeated category' do
          expect(response.body).to match(/La validación falló: Descripción ya ha sido tomado/)
        end
      end
    end
  end

  describe 'DELETE /incident_categories/:id' do
    before { delete "/incident_categories/#{category_id}" }

    it 'removes category and returns status code 204' do
      expect(response).to have_http_status 204
    end
  end
end
