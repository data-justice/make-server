require 'rails_helper'

RSpec.describe 'Authorization', type: :request do
  context 'when UNCONFIRMED' do
    let!(:user) { create :user }
    let(:valid_params) do
      {
        user: {
          email: user.email,
          password: user.password,
          username: user.username
        }
      }
    end
    let(:invalid_params) do
      {
        user: {
          username: user.username,
          password: 'xx'
        }
      }
    end

    context 'when VALID' do
      before { post '/login', params: valid_params }

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end

      it 'returns validation message' do
        expect(response.body).to match(/Tienes que confirmar tu cuenta antes de continuar/)
      end
    end

    context 'when INVALID' do
      before { post '/login', params: invalid_params }

      it 'returns validation message' do
        expect(response.body).to match(/Credenciales inválidas/)
      end
    end
  end
  context 'when CONFIRMED' do
    let!(:user) { create :user, :confirmed }
    let(:valid_params) do
      {
        user: {
          username: user.username,
          password: user.password
        }
      }
    end

    context 'when VALID' do
      describe 'POST /login' do
        before { post '/login', params: valid_params }

        it 'returns status code 201' do
          expect(response).to have_http_status(201)
        end

        it 'includes JWT Token both in headers and body' do
          #expect(response.headers['Authorization']).not_to be_empty
          expect(json).not_to be_empty
          expect(json['token']).not_to be_empty
          expect(json['id']).not_to be_nil
        end
      end
    end

    context 'when INVALID' do
      before { post '/login', params: { user: { username: user.username,  password: 'x' } } }

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end

      it 'returns validation message' do
        expect(response.body).to match(/Credenciales inválidas/)
      end
    end
  end
end
