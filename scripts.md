# Postgres

Create user:
```
psql
createuser mke;
alter user USER with encrypted password PASSWORD;
alter role USER superuser createdb replication login;
grant all privileges on database DATABASE to USER;
create database DATABASE owner USER;
```
