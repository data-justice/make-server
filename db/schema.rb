# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_26_232338) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file"
    t.bigint "user_id"
    t.index ["file"], name: "index_attachments_on_file"
    t.index ["user_id"], name: "index_attachments_on_user_id"
  end

  create_table "attachments_incident_complements", id: false, force: :cascade do |t|
    t.bigint "incident_complement_id", null: false
    t.bigint "attachment_id", null: false
    t.index ["attachment_id"], name: "index_attachments_incident_complements_on_attachment_id"
    t.index ["incident_complement_id", "attachment_id"], name: "index_complement_incident_attachment", unique: true
    t.index ["incident_complement_id"], name: "index_complement_incident"
  end

  create_table "attachments_incidents", id: false, force: :cascade do |t|
    t.bigint "incident_id", null: false
    t.bigint "attachment_id", null: false
    t.index ["attachment_id", "incident_id"], name: "index_attachments_incidents_on_attachment_id_and_incident_id"
  end

  create_table "group_memberships", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_id", null: false
    t.boolean "pending", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "vote_weight", default: 1, null: false
    t.index ["group_id"], name: "index_group_memberships_on_group_id"
    t.index ["user_id", "group_id"], name: "index_group_memberships_on_user_id_and_group_id", unique: true
    t.index ["user_id"], name: "index_group_memberships_on_user_id"
  end

  create_table "group_roles", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_group_roles_on_name", unique: true
  end

  create_table "group_roles_users", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_role_id", null: false
    t.bigint "security_plan_id", null: false
    t.datetime "created_at", default: "2020-02-17 22:59:13", null: false
    t.datetime "updated_at", default: "2020-02-17 22:59:13", null: false
    t.index ["group_role_id"], name: "index_group_roles_users_on_group_role_id"
    t.index ["security_plan_id"], name: "index_group_roles_users_on_security_plan_id"
    t.index ["user_id", "security_plan_id", "group_role_id"], name: "user_security_role"
    t.index ["user_id"], name: "index_group_roles_users_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.integer "governance_model", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.bigint "active_security_plan_id"
    t.bigint "centralized_person_id"
    t.boolean "isOwn", default: false
    t.index ["active_security_plan_id"], name: "index_groups_on_active_security_plan_id"
    t.index ["centralized_person_id"], name: "index_groups_on_centralized_person_id"
    t.index ["user_id"], name: "index_groups_on_user_id"
  end

  create_table "incident_attachments", force: :cascade do |t|
    t.bigint "incident_id"
    t.bigint "attachment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_id"], name: "index_incident_attachments_on_attachment_id"
    t.index ["incident_id"], name: "index_incident_attachments_on_incident_id"
  end

  create_table "incident_categories", force: :cascade do |t|
    t.string "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["description"], name: "index_incident_categories_on_description", unique: true
  end

  create_table "incident_complements", force: :cascade do |t|
    t.string "title", null: false
    t.string "description", null: false
    t.date "date", null: false
    t.time "time", null: false
    t.bigint "incident_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["incident_id"], name: "index_incident_complements_on_incident_id"
  end

  create_table "incidents", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.date "date"
    t.time "time"
    t.string "location_address"
    t.string "location_state"
    t.string "location_city"
    t.string "location_zip_code"
    t.string "location_country"
    t.float "location_latitude"
    t.float "location_longitude"
    t.string "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_type"
    t.bigint "incident_category_id"
    t.bigint "user_id"
    t.bigint "group_id"
    t.index ["group_id"], name: "index_incidents_on_group_id"
    t.index ["incident_category_id"], name: "index_incidents_on_incident_category_id"
    t.index ["user_id"], name: "index_incidents_on_user_id"
  end

  create_table "jwt_blacklists", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.index ["jti"], name: "index_jwt_blacklists_on_jti"
  end

  create_table "poll_choices", force: :cascade do |t|
    t.string "title", null: false
    t.bigint "poll_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["poll_id"], name: "index_poll_choices_on_poll_id"
    t.index ["title", "poll_id"], name: "index_poll_choices_on_title_and_poll_id", unique: true
  end

  create_table "polls", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id", null: false
    t.string "title", null: false
    t.text "description", null: false
    t.boolean "is_governance_votation", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_polls_on_group_id"
    t.index ["user_id"], name: "index_polls_on_user_id"
  end

  create_table "security_plans", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "missing_person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id", "missing_person_id"], name: "index_security_plans_on_group_id_and_missing_person_id", unique: true
    t.index ["group_id"], name: "index_security_plans_on_group_id"
    t.index ["group_id"], name: "sec_plan_stable_index", unique: true, where: "(missing_person_id IS NULL)"
    t.index ["missing_person_id"], name: "index_security_plans_on_missing_person_id"
  end

  create_table "user_governance_votes", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "security_plan_id", null: false
    t.integer "governance_model", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["security_plan_id"], name: "index_user_governance_votes_on_security_plan_id"
    t.index ["user_id", "security_plan_id"], name: "index_user_governance_votes_on_user_id_and_security_plan_id", unique: true
    t.index ["user_id"], name: "index_user_governance_votes_on_user_id"
  end

  create_table "user_votes", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "poll_id", null: false
    t.bigint "poll_choice_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["poll_choice_id"], name: "index_user_votes_on_poll_choice_id"
    t.index ["poll_id"], name: "index_user_votes_on_poll_id"
    t.index ["user_id", "poll_id"], name: "index_user_votes_on_user_id_and_poll_id", unique: true
    t.index ["user_id"], name: "index_user_votes_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "username", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "zip_codes", force: :cascade do |t|
    t.string "d_codigo"
    t.string "d_asenta"
    t.string "d_tipo_asenta"
    t.string "D_mnpio"
    t.string "d_estado"
    t.string "d_ciudad"
    t.string "d_CP"
    t.string "c_estado"
    t.string "c_oficina"
    t.string "c_CP"
    t.string "c_tipo_asenta"
    t.string "c_mnpio"
    t.string "id_asenta_cpcons"
    t.string "d_zona"
    t.string "c_cve_ciudad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["d_codigo"], name: "index_zip_codes_on_d_codigo"
  end

  add_foreign_key "attachments", "users"
  add_foreign_key "group_memberships", "groups"
  add_foreign_key "group_memberships", "users"
  add_foreign_key "group_roles_users", "group_roles"
  add_foreign_key "group_roles_users", "security_plans"
  add_foreign_key "group_roles_users", "users"
  add_foreign_key "groups", "security_plans", column: "active_security_plan_id"
  add_foreign_key "groups", "users", column: "centralized_person_id"
  add_foreign_key "incident_attachments", "attachments"
  add_foreign_key "incident_attachments", "incidents"
  add_foreign_key "incident_complements", "incidents"
  add_foreign_key "incidents", "groups"
  add_foreign_key "incidents", "incident_categories"
  add_foreign_key "incidents", "users"
  add_foreign_key "poll_choices", "polls"
  add_foreign_key "polls", "groups"
  add_foreign_key "polls", "users"
  add_foreign_key "security_plans", "groups"
  add_foreign_key "security_plans", "users", column: "missing_person_id"
  add_foreign_key "user_governance_votes", "security_plans"
  add_foreign_key "user_governance_votes", "users"
  add_foreign_key "user_votes", "poll_choices"
  add_foreign_key "user_votes", "polls"
  add_foreign_key "user_votes", "users"
end
