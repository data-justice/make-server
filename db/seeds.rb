require 'json'
require 'csv'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Incident Categories
jsons = [
  ['group_roles.json', GroupRole],
  ['incident_categories.json', IncidentCategory],
]

jsons.each do |collection|
  filename, _CollectionModel = collection
  filepath = File.join( Rails.root, 'db', 'jsons', filename )
  data = JSON.parse(File.read(filepath))

  result = _CollectionModel.create(data)
  puts "Imported data for #{_CollectionModel.model_name.name}"
  puts "\tInserts: #{result.size}"
end


csvs = [
  ['MexicoZipCodes.csv', ZipCode]
]

csvs.each do |collection|
  filename, _CollectionModel = collection
  filepath = File.join(Rails.root, 'db', 'csvs', filename)
  items = []
  i = 1
  CSV.foreach(filepath, headers: true) do |row|
    item = row.to_h
    item['id'] = i
    items << item
    i += 1
  end
  result = _CollectionModel.import(items)
  puts "Imported data for #{_CollectionModel.model_name.name}"
  puts "\tInserts: #{result.ids.size}"
  puts "\tFailed: #{result.failed_instances.size}"
end

ActiveRecord::Base.connection.tables.each do |t|
  ActiveRecord::Base.connection.reset_pk_sequence!(t)
end
