class CreateGroupRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :group_roles do |t|
      t.string :name, unique: true, null: false

      t.index :name, unique: true

      t.timestamps
    end
  end
end
