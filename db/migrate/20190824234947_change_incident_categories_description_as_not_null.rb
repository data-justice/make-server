class ChangeIncidentCategoriesDescriptionAsNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :incident_categories, :description, false
    add_index :incident_categories, :description, unique: true
  end
end
