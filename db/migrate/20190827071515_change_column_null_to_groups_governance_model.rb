class ChangeColumnNullToGroupsGovernanceModel < ActiveRecord::Migration[5.2]
  def change
    change_column_null :groups, :governance_model, false
    change_column_null :groups, :name, false
  end
end
