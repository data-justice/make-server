class CreateJoinTableIncidentComplementAttachment < ActiveRecord::Migration[5.2]
  def change
    create_join_table :incident_complements, :attachments do |t|
      t.references :incident_complement, null: false, index: { name: 'index_complement_incident' }
      t.belongs_to :attachment, null: false

      t.index [:incident_complement_id, :attachment_id], unique: true, name: 'index_complement_incident_attachment'
    end
  end
end
