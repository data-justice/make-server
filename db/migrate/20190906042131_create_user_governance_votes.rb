class CreateUserGovernanceVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :user_governance_votes do |t|
      t.references :user, foreign_key: true, null: false
      t.references :security_plan, foreign_key: true, null: false
      t.integer :governance_model, null: false

      t.index [:user_id, :security_plan_id], unique: true

      t.timestamps
    end

    userSecs = UserGroupRole.select(:user_id, :security_plan_id).distinct(:security_plan_id)
    votes = userSecs.map do |sec|
      { user_id: sec.user_id, security_plan_id: sec.security_plan_id, governance_model: Group.governance_models[:democratic] }
    end
    UserGovernanceVote.create!(votes)
  end
end
