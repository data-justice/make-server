class CreateStableSecPlans < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|

      dir.up do
        Group.ids.each do |group_id|
          puts "Creating Stable for Group #{group_id}"
          SecurityPlan.create! missing_person: nil, group_id: group_id
        end
      end

      dir.down do
        puts "Removing Stable for Groups"
        SecurityPlan.where(missing_person: nil).delete_all
      end
    end
  end
end
