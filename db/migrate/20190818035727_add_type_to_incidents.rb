class AddTypeToIncidents < ActiveRecord::Migration[5.2]
  def change
    add_column :incidents, :type, :integer
  end
end
