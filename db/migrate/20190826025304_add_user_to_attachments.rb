class AddUserToAttachments < ActiveRecord::Migration[5.2]
  def change
    add_reference :attachments, :user, foreign_key: true, index: true
  end
end
