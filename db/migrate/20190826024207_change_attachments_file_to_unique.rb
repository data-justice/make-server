class ChangeAttachmentsFileToUnique < ActiveRecord::Migration[5.2]
  def change
    add_index :attachments, :file, unique: true
  end
end
