class AddIndexSecPlanNull < ActiveRecord::Migration[5.2]
  def change
    add_index :security_plans, :group_id, unique: true, where: 'missing_person_id is null', name: 'sec_plan_stable_index'
  end
end
