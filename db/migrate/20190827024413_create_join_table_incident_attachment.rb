class CreateJoinTableIncidentAttachment < ActiveRecord::Migration[5.2]
  def change
    create_join_table :incidents, :attachments do |t|
      t.index [:attachment_id, :incident_id]
    end
  end
end
