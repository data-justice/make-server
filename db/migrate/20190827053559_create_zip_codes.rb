class CreateZipCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :zip_codes do |t|
      t.string :d_codigo
      t.string :d_asenta
      t.string :d_tipo_asenta
      t.string :D_mnpio
      t.string :d_estado
      t.string :d_ciudad
      t.string :d_CP
      t.string :d_CP
      t.string :c_estado
      t.string :c_oficina
      t.string :c_CP
      t.string :c_tipo_asenta
      t.string :c_mnpio
      t.string :id_asenta_cpcons
      t.string :d_zona
      t.string :c_cve_ciudad

      t.timestamps
    end

    add_index :zip_codes, :d_codigo
  end
end
