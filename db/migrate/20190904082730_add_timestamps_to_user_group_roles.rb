class AddTimestampsToUserGroupRoles < ActiveRecord::Migration[5.2]
  def change
    add_timestamps :group_roles_users, default: Time.zone.now
  end
end
