class CreateUserVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :user_votes do |t|
      t.references :user, foreign_key: true, null: false
      t.references :poll, foreign_key: true, null: false
      t.references :poll_choice, foreign_key: true, null: false

      t.index [:user_id, :poll_id], unique: true

      t.timestamps
    end
  end
end
