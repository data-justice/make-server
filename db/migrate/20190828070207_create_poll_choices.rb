class CreatePollChoices < ActiveRecord::Migration[5.2]
  def change
    create_table :poll_choices do |t|
      t.string :title, null: false
      t.references :poll, foreign_key: true, null: false

      t.index [:title, :poll_id], unique: true

      t.timestamps
    end
  end
end
