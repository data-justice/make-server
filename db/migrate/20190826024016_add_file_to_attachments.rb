class AddFileToAttachments < ActiveRecord::Migration[5.2]
  def change
    add_column :attachments, :file, :string, null: false, unique: true
  end
end
