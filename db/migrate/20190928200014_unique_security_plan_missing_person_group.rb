class UniqueSecurityPlanMissingPersonGroup < ActiveRecord::Migration[5.2]
  def change
    add_index :security_plans, [:group_id, :missing_person_id], unique: true
  end
end
