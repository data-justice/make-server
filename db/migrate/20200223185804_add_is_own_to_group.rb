# frozen_string_literal: true

class AddIsOwnToGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :groups, :isOwn, :boolean, default: false
  end
end
