class CreateJoinTableUserSecurityPlan < ActiveRecord::Migration[5.2]
  def change
    create_join_table :users, :group_roles do |t|
      t.references :security_plan, null: false, foreign_key: true
      t.references :group_role, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.index [:user_id, :security_plan_id, :group_role_id], name: 'user_security_role'
    end
  end
end
