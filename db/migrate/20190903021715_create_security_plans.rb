class CreateSecurityPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :security_plans do |t|
      t.references :group, foreign_key: true, null: false
      t.references :missing_person, index: true, foreign_key: { to_table: :users }, null: true

      t.timestamps
    end
  end
end
