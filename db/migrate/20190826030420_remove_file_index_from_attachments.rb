class RemoveFileIndexFromAttachments < ActiveRecord::Migration[5.2]
  def change
    remove_index :attachments, :file
    add_index :attachments, :file
  end
end
