class RenameUsersTypeToEventType < ActiveRecord::Migration[5.2]
  def change
    rename_column :incidents, :type, :event_type
  end
end
