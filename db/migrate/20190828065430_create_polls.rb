class CreatePolls < ActiveRecord::Migration[5.2]
  def change
    create_table :polls do |t|
      t.references :group, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false
      t.string :title, null: false
      t.text :description, null: false
      t.boolean :is_governance_votation, default: false

      t.timestamps
    end
  end
end
