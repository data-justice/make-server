class CreateIncidentAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :incident_attachments do |t|
      t.references :incident, foreign_key: true
      t.references :attachment, foreign_key: true

      t.timestamps
    end
  end
end
