class AddForeingKeysToMembership < ActiveRecord::Migration[5.2]
  def change
    change_column_default :group_memberships, :pending, from: nil, to: true

    change_column_null :group_memberships, :pending, false
    change_column_null :group_memberships, :user_id, false
    change_column_null :group_memberships, :group_id, false
  end
end
