class CreateIncidents < ActiveRecord::Migration[5.2]
  def change
    create_table :incidents do |t|
      t.string :title
      t.text :description
      t.date :date
      t.time :time
      t.string :location_address
      t.string :location_state
      t.string :location_city
      t.string :location_zip_code
      t.string :location_country
      t.float :location_latitude
      t.float :location_longitude
      t.string :created_by

      t.timestamps
    end
  end
end
