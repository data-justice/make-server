class RemoveFileNullFromAttachments < ActiveRecord::Migration[5.2]
  def change
    change_column_null :attachments, :file, true
  end
end
