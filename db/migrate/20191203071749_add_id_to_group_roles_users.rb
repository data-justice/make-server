class AddIdToGroupRolesUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :group_roles_users, :id, :primary_key
  end
end
