class CreateIncidentComplements < ActiveRecord::Migration[5.2]
  def change
    create_table :incident_complements do |t|
      t.string :title, null: false
      t.string :description, null: false
      t.date :date, null: false
      t.time :time, null: false
      t.references :incident, foreign_key: true, null: false

      t.timestamps
    end
  end
end
