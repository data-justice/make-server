class AddActiveSecurityPlanToGroups < ActiveRecord::Migration[5.2]
  def change
    add_reference :groups, :active_security_plan, foreign_key: { to_table: :security_plans }, references: :security_plan, index: true
  end
end
