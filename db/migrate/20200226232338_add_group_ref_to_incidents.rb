class AddGroupRefToIncidents < ActiveRecord::Migration[5.2]
  def change
    add_reference :incidents, :group, foreign_key: true
  end
end
