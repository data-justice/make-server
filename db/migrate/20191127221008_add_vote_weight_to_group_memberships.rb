class AddVoteWeightToGroupMemberships < ActiveRecord::Migration[5.2]
  def change
    add_column :group_memberships, :vote_weight, :integer, default: 1, null: false
  end
end
