class AddCreatorToGroups < ActiveRecord::Migration[5.2]
  def change
    add_reference :groups, :user, index: true, null: false
  end
end
