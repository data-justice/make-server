class AddCentralizedPersonColumnToGroups < ActiveRecord::Migration[5.2]
  def change
    add_reference :groups, :centralized_person, foreign_key: { to_table: :users }
  end
end
