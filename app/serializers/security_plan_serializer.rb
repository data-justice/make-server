class SecurityPlanSerializer < ActiveModel::Serializer
  attributes :id

  # belongs_to :group
  # belongs_to :missing_person, key: :missing_person

  attribute :missing_person do
    person = object.missing_person
    person && person.username
  end
  attribute :is_stable do
    object.stable?
  end

  attribute :members_roles do
    members = object.user_group_roles
    result = {}

    members.each do |user_group_role|
      username = user_group_role.user.username
      result[username] ||= []
      result[username] << { role: user_group_role.group_role.name, role_id: user_group_role.group_role.id }
    end

    result
  end

  attribute :governance_details do
    object.votes
  end

  attribute :group_user_weights do
    object.group.user_weights
  end
  attribute :group_centralized_person do
    user = object.group.centralized_person
    user.username if user
  end
end
