# frozen_string_literal: true

class IncidentSerializer < ActiveModel::Serializer
  attributes :id,
             # Relations
             :user,
             :category,
             :category_id,
             # Fields
             :title,
             :description,
             :date,
             :location_address,
             :location_city,
             :location_state,
             :location_zip_code,
             :location_country,
             :location_latitude,
             :location_longitude,
             # Computed
             #:regular_event,
             #:panic_event,
             :created_at,
             :updated_at

  has_many :attachments

  attribute :category_id do
    object.incident_category.id
  end

  attribute :group_id do
    object.group&.id
  end

  attribute :group do
    object.group&.name
  end

  attribute :category do
    object.incident_category.description
  end

  attribute :user do
    object.user.username
  end

  attribute :panic_event do
    object.panic_event?
  end
  attribute :regular_event do
    object.regular_event?
  end

  attribute :time do
    object.time.strftime '%H:%M'
  end

  class IncidentComplementSerializer < ActiveModel::Serializer
    attributes :id, :title, :description, :date
    attribute :time do
      object.time.strftime '%H:%M'
    end
    attribute :attachments do
      ActiveModelSerializers::SerializableResource.new(object.attachments)
    end
  end

  has_many :incident_complements, key: :complements, serializer: IncidentComplementSerializer, each_serializer: IncidentComplementSerializer
end
