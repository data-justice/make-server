class UserVoteSerializer < ActiveModel::Serializer
  attributes :id, :poll_id, :user_id, :poll_choice_id

  attribute :message do
    choice = object.poll_choice.title
    "Votaste correctamente por '#{choice}'"
  end

  attribute :topic do
    object.poll.title
  end
  
  attributes :created_at, :updated_at
end
