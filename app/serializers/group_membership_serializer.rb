class GroupMembershipSerializer < ActiveModel::Serializer
  attributes :pending

  attribute :membership_id do
    object.id
  end

  belongs_to :group

  #class GroupSerializer < ActiveModel::Serializer
    #attribute :creator do
      #object.user.username
    #end
    #attribute :creator_id do
      #object.user.id
    #end
  #end

  attributes :created_at, :updated_at
end
