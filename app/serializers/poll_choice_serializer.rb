class PollChoiceSerializer < ActiveModel::Serializer
  attributes :id, :title
end
