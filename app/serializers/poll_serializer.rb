class PollSerializer < ActiveModel::Serializer
  attributes :id, :title, :description

  attribute :group do
    object.group.id
  end
  attribute :group_name do
    object.group.name
  end
  attribute :creator do
    object.user.username
  end
  attribute :creator_id do
    object.user.id
  end

  has_many :poll_choices, key: :choices

  class UserVoteSerializer < ActiveModel::Serializer
    attribute :id, key: :vote_id
    attribute :user do
      object.user.username
    end
    attribute :vote_choice do
      object.poll_choice
    end
  end
  has_many :user_votes, key: :votes

  attributes :created_at, :updated_at
end
