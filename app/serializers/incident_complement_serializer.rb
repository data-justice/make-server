# frozen_string_literal: true

class IncidentComplementSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :date

  attribute :incident
  has_many :attachments

  attribute :time do
    object.time.strftime '%H:%M'
  end
end
