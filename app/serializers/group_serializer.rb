# frozen_string_literal: true

class GroupSerializer < ActiveModel::Serializer
  class UserSerializer < ActiveModel::Serializer
    attributes :id, :username
  end

  has_many :users, serializer: UserSerializer, key: :members
  # attribute :users, serializer: UserSerializer

  attributes :id, :name, :governance_model, :active_security_plan_id

  attribute :creator do
    object.user.username
  end
  attribute :creator_id do
    object.user.id
  end

  attribute :members_count do
    object.users.count
  end

  belongs_to :active_security_plan
  attribute :centralized_user do
    object.centralized_person&.username
  end

  attribute :governance_model do
    sec_plan = object.active_security_plan
    (sec_plan && sec_plan.votes[:winner]) || 'democratic'
  end

  attribute :stable
  attribute :user_weights
  # attribute :active_security_plan do
  #   # serialized_sec_plan = ActiveModelSerializers::SerializableResource.new(security_plan)
  #   object.active_security_plan
  # end

  attribute :is_own do
    object.isOwn
  end
end
