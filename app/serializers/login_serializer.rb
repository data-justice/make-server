class LoginSerializer < ActiveModel::Serializer
  attributes :id, :username, :email, :token, :updated_at
end
