class AttachmentSerializer < ActiveModel::Serializer
  attributes :id

  attribute :user do
    object.user.username
  end

  attribute :file do
    object.file.path
  end

  attribute :file_identifier do
    object.file.identifier
  end

  attribute :file_url do
    object.file.url
  end
end
