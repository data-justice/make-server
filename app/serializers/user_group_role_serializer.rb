class UserGroupRoleSerializer < ActiveModel::Serializer
  # belongs_to :user
  # belongs_to :group_role
  # belongs_to :security_plan

  attribute :security_plan_id
  attribute :id do
    object.group_role.id
  end
  attribute :name do
    object.group_role.name
  end
end
