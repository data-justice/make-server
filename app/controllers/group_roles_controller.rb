class GroupRolesController < ApplicationController
  def create
    role = GroupRole.create!(create_params)
    render json: role, status: :created
  end

  def delete
    role = GroupRole.find params[:id]

    role.destroy!

    head :no_content
  end

  def index
    roles = GroupRole.all

    render json: roles
  end

  private

  def create_params
    params.permit(:name)
  end
end
