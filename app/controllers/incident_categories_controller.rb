class IncidentCategoriesController < ApplicationController
   before_action :set_category, only: [:show, :update, :destroy]

  # GET /incident_categories
  def index
    @categories = IncidentCategory.all
    json_response(@categories)
  end

  # POST /incident_categories
  def create
    @category = IncidentCategory.create!(category_params)
    json_response @category, :created
  end

  # DELETE /incident_categories/:id
  def destroy
    @category.destroy
    head :no_content
  end

  protected

  def category_params
    params.permit :description
  end

  def set_category
    @category = IncidentCategory.find(params[:id])
  end
end
