class ZipCodesController < ApplicationController
  def show
    zip_codes = ZipCode.where(d_codigo: params[:zip_code])

    if zip_codes.empty?
      render json: zip_codes, status: :not_found and return
    end

    render json: zip_codes
  end
end
