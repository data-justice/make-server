class PanicEventController < ApplicationController
  before_action :authenticate_user!

  # POST /user/panic_event
  def create
    panic_event = Incident.new event_type: :panic_event
    panic_event.title = "Evento de pánico #{Date.now}"
    panic_event.description = "Evento creado a través del botón de pánico."
    panic_event.save!

    render json: panic_event, status: :created
  end
end
