# frozen_string_literal: true

require 'application_responder'

class ApplicationController < ActionController::API
  self.responder = ApplicationResponder
  respond_to :json

  include Response
  include ExceptionHandler

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  before_action :set_raven_context

  private

  def set_raven_context
    user = current_user
    username = user.username if user
    Raven.user_context(id: username)
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end
end
