class User::IncidentComplementsController < ApplicationController
  before_action :authenticate_user!

  def create
    _params  = create_params
    _attachment_ids = _params.delete :attachments
    _incident = _params.delete :incident

    @incident = IncidentComplement.new _params
    @incident.incident_id = _incident
    @incident.attachment_ids = _attachment_ids
    # @incident.user = current_user

    @incident.save!

    render json: @incident, status: :created
  end

  def index
    incidents = IncidentComplement.where incident_id: params[:incident_id]
    render json: incidents
  end


  private

  def create_params
    params.permit(:title, :description, :date, :time, :incident, attachments: [])
  end
end
