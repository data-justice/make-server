class User::PollsController < ApplicationController
  before_action :authenticate_user!
  before_action :prepare_create_params, only: [:create]

  def index
    polls = Poll.where user: current_user

    render json: polls
  end

  def create
    _params = create_params
    puts "PARAMS: #{_params}"
    poll = Poll.new _params
    poll.user = current_user
    poll.save!

    render json: poll, status: :created
  end

  def vote
    _params = vote_params

    vote = UserVote.new
    vote.user = current_user
    vote.poll_id = _params[:topic]
    vote.poll_choice_id = _params[:choice]

    vote.save!

    render json: vote, status: :created
  end

  private

  def vote_params
    params.permit(:topic, :choice)
  end

  def prepare_create_params
    params[:poll_choices_attributes] = params.delete(:choices)
    params[:group_id] = params.delete(:group)
  end

  def create_params
    params.permit(:group, :group_id, :title, :description, poll_choices_attributes: [:title])
  end
end
