class User::GroupPollsController < ApplicationController
  before_action :authenticate_user!

  def index
    polls = Poll.where group_id: params[:group_id]

    render json: polls, status: :ok
  end
end
