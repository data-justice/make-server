class User::AttachmentsController < ApplicationController
  before_action :authenticate_user!

  def index
    attachments = current_user.attachments

    render json: attachments, status: :ok
  end

  def create
    @attachment = Attachment.new
    @attachment.file = params[:file]
    @attachment.user = current_user
    @attachment.save!

    render json: @attachment, status: :created
  end

  def destroy
  end

  protected
  def attachment_params
    params.permit(:file)
  end
end
