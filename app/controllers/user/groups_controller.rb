# frozen_string_literal: true

class User::GroupsController < ApplicationController
  before_action :authenticate_user!

  def index
    groups = current_user.groups

    render json: groups
  end

  def show
    group = Group.find params[:id]

    render json: group, status: :ok
  end

  def create
    _params = create_params

    group = Group.new
    group.user = current_user
    group.name = _params[:name]
    group.members = _params.delete :members
    group.save!

    render json: group, status: :created
  end

  def update
    group = Group.find params[:id]
    _params = update_params

    if _params[:members]
      unless _params[:members].include? current_user.username
        _params[:members] << current_user.username
      end

      new_members = User.where username: _params[:members]
      group.users = new_members
    end
    group.update! _params

    render json: group, status: :ok
  end

  def set_security_plan
    _params = set_security_plan_params
    group = Group.find _params[:group_id]
    if _params[:stable]
      group.update! active_security_plan: group.stable_security_plan, governance_model: Group.governance_models[:democratic]
      return render json: group, status: :ok
    end
    security_plan = SecurityPlan.missing_person_in_group(_params[:missing_person], group.id).first
    serialized_sec_plan = ActiveModelSerializers::SerializableResource.new(security_plan)

    winner = security_plan.votes[:winner]

    unless security_plan
      return render json: { message: 'Plan de seguridad no encontrado' }, status: :not_found
    end

    group.update! active_security_plan: security_plan, governance_model: winner

    serialized_group = ActiveModelSerializers::SerializableResource.new(group)

    result = {
      group: serialized_group,
      active_security_plan: serialized_sec_plan
    }

    render json: result, status: :ok
  end

  def set_gov_centralized_person
    username = centralized_person_params[:user]
    user = username.nil? ? nil : User.find_by!(username: username)

    group = Group.find params[:group_id]

    group.centralized_person = user
    group.save

    render json: group, status: :ok
  end

  def set_gov_user_weights
    puts user_weights_params
    _params = user_weights_params
    g = Group.find params[:group_id]
    memberships = g.group_memberships.map { |m| [m.user.username, m] }.to_h

    g.transaction do
      _params.each do |username, weight|
        membership = memberships[username]
        next unless membership

        membership.vote_weight = weight.to_i
        membership.save!
      end
    end

    render json: g, status: :ok
  end

  def add_member
    group = Group.find params[:group_id]
    username = params[:username]
    user = User.find_by! username: username

    membership = GroupMembership.find_by user: user, group: group

    if membership
      return render json: { message: "El miembro '#{username}' ya existe en la isla" }, status: :unprocessable_entity
    end

    membership = GroupMembership.create!(user: user, group: group)
    render json: membership, status: :created
  end

  def remove_member
    username = params[:username]
    group_id = params[:group_id]

    group = Group.find group_id
    user = User.find_by! username: username
    membership = GroupMembership.find_by! user: user, group: group

    security_plan = SecurityPlan.find_by missing_person: user, group: group

    if security_plan && (group.active_security_plan == security_plan)
      return render json: { message: 'No se puede eliminar porque el grupo tiene el plan de seguridad activo para este miembro.' }, status: :unprocessable_entity
    end

    membership.destroy!

    render status: :no_content
  end

  private

  def user_weights_params
    params.require(:weights).permit!
  end

  def centralized_person_params
    params.permit(:user)
  end

  def create_params
    params.permit(:name, members: [])
  end

  def update_params
    params.permit(:name, :security_plan, members: [])
  end

  def set_security_plan_params
    params.permit(:missing_person, :group_id, :stable)
  end
end
