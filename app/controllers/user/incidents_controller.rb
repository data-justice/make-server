# frozen_string_literal: true

class User::IncidentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_incident, only: %i[show destroy]

  def index
    @incidents = Incident.where(user: current_user).order(date: :desc, time: :desc) || []
    json_response @incidents
  end

  def create
    _params = create_params

    is_panic_event = params[:panic_event]
    category_name = _params.delete :category
    category = IncidentCategory.find_by_description! category_name

    _attachment_ids = _params.delete :attachments

    @incident = Incident.new(_params)
    @incident.user = current_user
    @incident.attachment_ids = _attachment_ids
    @incident.incident_category = category

    # Detection of panic event
    if is_panic_event
      @incident.event_type = :panic_event
      @incident.title ||= "Evento de Pánico creado el #{Date.today}"
      @incident.description ||= 'Evento creado a través del botón de pánico.'
    end

    @incident.save!

    render json: @incident, status: :created
  end

  def show
    render json: @incident
  end

  def destroy
    @incident.destroy

    head :no_content
  end

  private

  def set_incident
    @incident = Incident.find params[:id]
  end

  def create_params
    params.permit(
      :title,
      :description,
      :category,
      :description,
      :group_id,
      :date,
      :time,
      :location_latitude,
      :location_longitude,
      :location_address,
      :location_state,
      :location_city,
      :location_country,
      :location_zip_code,
      attachments: []
    )
  end
end
