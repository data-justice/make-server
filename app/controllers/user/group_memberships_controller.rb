class User::GroupMembershipsController < ApplicationController
  before_action :authenticate_user!

  def index
    memberships = GroupMembership.includes(:group, :user).where(user: current_user).group_by(&:pending)

    pending = memberships[true] || []
    active = memberships[false] || []

    pending_serialized = ActiveModelSerializers::SerializableResource.new(pending)
    active_serialized = ActiveModelSerializers::SerializableResource.new(active)

    result = { pending: pending_serialized || [], active: active_serialized || [] }
    render json: result
  end

  def update
    membership = GroupMembership.find params[:id]
    membership.update_attributes! update_params

    render json: membership
  end

  private

  def update_params
    params.permit :pending
  end
end
