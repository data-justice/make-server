# frozen_string_literal: true

class User::GroupIncidentsController < ApplicationController
  before_action :authenticate_user!

  def index
    group = Group.find params[:group_id]
    incidents = Incident.where(group: group).order(date: :desc, time: :desc)

    render json: incidents, status: :ok
  end
end
