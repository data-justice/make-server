# frozen_string_literal: true

class User::SecurityPlansController < ApplicationController
  before_action :authenticate_user!

  def template
    group             = Group.find(params[:group_id])
    roles             = GroupRole.all
    members           = group.group_memberships.collect { |m| m.user.username }
    governance_models = Group.governance_models.keys

    members.delete current_user.username
    governance_models.delete 'stable'

    render json: { group: group.name,
                   group_id: group.id,
                   available_members: members,
                   available_governance_models: governance_models,
                   available_roles: roles }
  end

  def create
    _params = create_params

    if !Group.governance_models.include? _params[:governance_model]
      valid_models = Group.governance_models.keys
      valid_models.delete 'stable'
      return render json: { message: "Modelo de gobernanza inválido. Disponibles: #{valid_models.join(", ")}" }, status: :unprocessable_entity
    end
    missing_person = User.find_by! username: _params[:missing_person]

    security_plan = SecurityPlan.find_by!(missing_person: missing_person,
                                          group: _params[:group])

    # Validate that there are no previous roles created
    existing_roles = UserGroupRole.where(user: current_user,
                                         security_plan: security_plan)

    unless existing_roles.empty?
      existing_roles_serialized = ActiveModelSerializers::SerializableResource.new(existing_roles)
      existing = {
        group: security_plan.group.id,
        group_name: security_plan.group.name,
        missing_person: missing_person.username,
        roles: existing_roles_serialized
      }

      return render json: { message: "Ya existe un plan de seguridad del usuario actual (#{current_user.username}) para la persona faltante (#{missing_person.username}).",
                            existing: existing }, status: :unprocessable_entity
    end

    created_roles = []
    _params[:roles].each do |role_id|
      created_roles << UserGroupRole.create!(user: current_user,
                                             security_plan: security_plan,
                                             group_role_id: role_id)
    end

    governance_vote = UserGovernanceVote.create! user: current_user,
                                                 security_plan: security_plan,
                                                 governance_model: Group.governance_models[params[:governance_model]]

    created_roles_serialized = ActiveModelSerializers::SerializableResource.new(created_roles)

    result = {
      group: security_plan.group.id,
      group_name: security_plan.group.name,
      security_plan_id: security_plan.id,
      missing_person: missing_person.username,
      governance_selected: governance_vote.governance_model,
      governance_vote_id: governance_vote.id,
      roles: created_roles_serialized
    }

    render json: result
  end


  def update
    _params = update_params
    security_plan = SecurityPlan.find_by!(group: params[:group_id], id: params[:id])

    governance_model = _params[:governance_model]
    if governance_model
      governance_vote = UserGovernanceVote.find_or_initialize_by user: current_user,
                                                   security_plan: security_plan
      governance_vote.governance_model = governance_model
      governance_vote.save!
    end

    role_ids = _params[:roles]
    if role_ids
      UserGroupRole.transaction do
        user_roles = UserGroupRole.where(user: current_user, security_plan: security_plan)
        user_roles.destroy_all

        role_ids.each do |role_id|
          UserGroupRole.create!(user: current_user,
                                security_plan: security_plan,
                                group_role_id: role_id)
        end
      end
    end


    return render json: security_plan
  end

  def show
    security_plan = SecurityPlan.find_by! group: params[:group_id], id: params[:id]
    return render json: security_plan
  end

  def index
    security_plans = SecurityPlan.where group: params[:group_id]

    render json: security_plans
  end

  def set_stable_options
    group = Group.find params[:group_id]
    security_plan = group.stable_security_plan

    governance_model = stable_params[:governance_model]

    if governance_model
      governance_vote = UserGovernanceVote.find_or_initialize_by user: current_user,
                                                                 security_plan: security_plan
      governance_vote.governance_model = governance_model
      governance_vote.save!
    end

    render json: security_plan
  end

  private

  def update_params
    params.permit(:governance_model, roles: [])
  end

  def create_params
    params.permit(:group, :missing_person, :governance_model, roles: [])
  end

  def stable_params
    params.permit(:governance_model)
  end
end
