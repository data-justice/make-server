# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  #before_action :configure_sign_in_params, only: [:create]
  respond_to :json


  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    super do |resource|
      @resource = resource
      @resource.token = current_token

      return render json: @resource, serializer: LoginSerializer, status: :created
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  protected
  def current_token
    request.env['warden-jwt_auth.token']
  end
end
