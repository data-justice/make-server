# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@mke-platform.sytes.net'
  layout 'mailer'
end
