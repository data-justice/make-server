class IncidentComplement < ApplicationRecord
  belongs_to :incident
  has_and_belongs_to_many :attachments

  validates_presence_of :title
  validates_presence_of :description
  validates_presence_of :date
  validates_presence_of :time
  validates_presence_of :incident
end
