class UserGroupRole < ApplicationRecord
  self.table_name = "group_roles_users"

  belongs_to :user
  belongs_to :security_plan
  belongs_to :group_role

  validates_presence_of :user
  validates_presence_of :security_plan
  validates_presence_of :group_role
end
