class UserGovernanceVote < ApplicationRecord
  belongs_to :user
  belongs_to :security_plan

  enum governance_model: Group.governance_models.keys.map(&:to_sym)

  validates_presence_of :user
  validates_presence_of :security_plan
  validates_presence_of :governance_model
end
