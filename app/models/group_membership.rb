class GroupMembership < ApplicationRecord
  belongs_to :user
  belongs_to :group

  validates_numericality_of :vote_weight, greater_than_or_equal_to: 0

  after_create :create_security_plan
  before_destroy :remove_security_plan
  private

  def create_security_plan
    SecurityPlan.find_or_create_by group: group, missing_person: user
  end

  def remove_security_plan
    security_plan = SecurityPlan.find_by missing_person: self.user, group: self.group
    security_plan.destroy! if security_plan
  end
end
