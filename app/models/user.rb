# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JWTBlacklist,
         authentication_keys: [:username]

  attribute :token, :string

  validates_presence_of :email
  validates_presence_of :username
  validates_presence_of :password_confirmation

  validates :email, uniqueness: true
  validates :username, uniqueness: true

  has_many :incidents
  has_many :attachments
  has_many :group_memberships
  has_many :groups, through: :group_memberships
  has_many :user_votes

  has_and_belongs_to_many :group_roles

  after_create :find_or_create_own_group

  def email_changed?
    false
  end

  def to_param
    username
  end

  def own_group
    find_or_create_own_group
  end

  private

  def find_or_create_own_group
    user = self
    group = Group.find_or_initialize_by(user_id: user.id, isOwn: true)
    if group.new_record?
      Group.transaction do
        group.user = user
        group.name = "Isla por defecto de #{username}"
        group.members = [username]
        group.save!
        membership = GroupMembership.find_by! user: user, group: group
        membership.pending = false
        membership.save!
      end
    end
    group
  end
end
