class IncidentCategory < ApplicationRecord
  has_many :incidents, dependent: :nullify

  validates_presence_of :description

  validates_uniqueness_of :description, case_sensitive: false

  scope :title, ->(name) { where(description: name) }
end
