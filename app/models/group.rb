# frozen_string_literal: true

class Group < ApplicationRecord
  enum governance_model: %i[democratic weighted centralized]

  attr_accessor :members

  after_initialize :set_defaults

  has_many :group_memberships
  has_many :users, through: :group_memberships
  has_many :security_plans

  accepts_nested_attributes_for :users, update_only: true

  accepts_nested_attributes_for :security_plans

  belongs_to :user
  belongs_to :active_security_plan, class_name: 'SecurityPlan', foreign_key: :active_security_plan_id, required: false
  belongs_to :centralized_person, class_name: 'User', foreign_key: :centralized_person_id, required: false

  validates_presence_of :name, :governance_model

  after_create :create_memberships
  after_create :create_stable_security_plan

  def stable
    active_security_plan&.stable?
  end

  def user_weights
    group_memberships.map { |m| [m.user.username, m.vote_weight] }.to_h
  end

  def stable_security_plan
    security_plans.find_by! missing_person: nil
  end

  private

  def set_defaults
    self.governance_model = :democratic unless governance_model
  end

  def create_memberships(members = self.members)
    return unless members
    return if members.empty?

    members << user.username unless members.include? user.username

    users = User.where(username: members)
    users.each do |user|
      GroupMembership.create! user: user, group: self
    end
  end

  def create_stable_security_plan
    stable_plan = security_plans.create! missing_person: nil
    update! active_security_plan: stable_plan, governance_model: Group.governance_models[:democratic]
  end
end
