class Poll < ApplicationRecord
  belongs_to :group
  belongs_to :user

  has_many :poll_choices

  has_many :user_votes

  validates_presence_of :user
  validates_presence_of :group
  validates_presence_of :title
  validates_presence_of :description
  validates_presence_of :poll_choices

  accepts_nested_attributes_for :poll_choices
end
