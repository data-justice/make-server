class SecurityPlan < ApplicationRecord
  belongs_to :group
  belongs_to :missing_person,  class_name: 'User', foreign_key: 'missing_person_id', optional: true

  validates_presence_of :group
  #validates_presence_of :missing_person

  has_many :user_group_roles, dependent: :delete_all

  has_many :user_governance_votes, dependent: :destroy

  # scope :group, ->(group_id) { where(group_id: group_id) }
  # scope :missing_person, ->(username) { where(missing_person__username: username) }sc
  scope :missing_person_in_group, ->(username, group_id) { joins(:missing_person)
                                                               .where(group_id: group_id,
                                                                      users: { username: username }) }

  def stable?
    self.missing_person.nil?
  end

  def votes
    counting = user_governance_votes.group(:governance_model).count
    result = {}
    Group.governance_models.keys.each do |g_model|
      result[g_model.to_sym] = counting[g_model] || 0
    end

    winner, winner_votes = result.max_by { |g_model, n_votes| n_votes }

    check_tie = result.select { |g_model, n_votes| n_votes == winner_votes }

    tie = false
    if check_tie.size > 1
      tie = true
    end

    details = self.votes_details

    result.delete :stable
    {
        votes: result,
        tie: tie,
        winner: winner,
        votes_details: details
    }
  end

  def votes_details
    votes = user_governance_votes.all
    details = {}
    votes.map do |vote|
      sym = vote.governance_model.to_sym
      details[sym] ||= []
      details[sym] << vote.user.username
    end
    Group.governance_models.keys.each do |g_model|
      details[g_model.to_sym] ||= []
    end
    details.delete :stable
    details
  end
end
