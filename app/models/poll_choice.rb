class PollChoice < ApplicationRecord
  belongs_to :poll

  validates_presence_of :poll
  validates_presence_of :title
end
