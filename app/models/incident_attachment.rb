class IncidentAttachment < ApplicationRecord
  belongs_to :incident
  belongs_to :attachment
end
