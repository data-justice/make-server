class Attachment < ApplicationRecord
  mount_uploader :file, IncidentAttachmentUploader

  validates_presence_of :file

  belongs_to :user
end
