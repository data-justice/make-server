class UserVote < ApplicationRecord
  belongs_to :user
  belongs_to :poll
  belongs_to :poll_choice

  validates_presence_of :user
  validates_presence_of :poll
  validates_presence_of :poll_choice

  validates_uniqueness_of :poll_id, scope: :user_id, message: "ya emitida anteriormente."
end
