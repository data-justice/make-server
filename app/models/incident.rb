# frozen_string_literal: true

class Incident < ApplicationRecord
  enum event_type: %i[regular_event panic_event]
  after_initialize :set_defaults

  belongs_to :incident_category
  belongs_to :user
  belongs_to :group, required: false

  has_and_belongs_to_many :attachments
  has_many :incident_complements, dependent: :destroy

  validates_presence_of :title, :description, :date, :time, :incident_category, :event_type
  validates_presence_of :user

  # Validations only for Panic Event
  validates_presence_of :location_address, :location_state, :location_city, :location_zip_code, :location_country, if: :regular_event?

  before_create :set_default_group

  private

  def set_default_group
    self.group = user.own_group unless group
  end

  def set_defaults
    self.event_type = :regular_event unless event_type
  end
end
