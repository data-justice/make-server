Raven.configure do |config|
  config.dsn = 'https://31e7aa69460c4768860f22bf872080ee:e8f60607770e4dd69819db3eb02b612f@sentry.io/1550806'
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
end

