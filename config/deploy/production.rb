# frozen_string_literal: true

# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

server 'make-sec.org', user: 'ubuntu', roles: %w[app db web]

# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
set :ssh_options,
    # keys: %w(/home/gomezhyuuga/.ssh/id_rsa),
    forward_agent: false,
    port: 4876,
    auth_methods: %w[publickey]
#
# The server-based syntax can be used to override options:
# ------------------------------------
# server "example.com",
#   user: "user_name",
#   roles: %w{web app},
#   ssh_options: {
#     user: "user_name", # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: "please use keys"
#   }

# PUMA CONFIGS
set :puma_threads, [2, 8]
set :puma_workers, 0
# Don't change these unless you know what you're doing
# set :pty,             true
# set :use_sudo,        false
set :stage, :production
set :deploy_via, :remote_cache
# set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind, ['tcp://0.0.0.0:3004', "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"]
# set :puma_stdout_redirect, "#{shared_path}/log/puma.stdout.log", "#{shared_path}/log/puma.stderr.log", true
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log, "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_conf, "#{shared_path}/puma.production.rb"
set :nginx_server_name, 'localhost make-sec.org'

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
      execute "mkdir #{shared_path}/log -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc 'reload the database with seed data'
  task :seed do
    on roles(:app) do
      with rails_env: :production do
        within release_path do
          execute :rake, 'db:seed'
        end
      end
    end
  end

  # desc "Make sure local git is in sync with remote."
  # task :check_revision do
  # on roles(:app) do
  # unless `git rev-parse HEAD` == `git rev-parse origin/master`
  # puts "WARNING: HEAD is not the same as origin/master"
  # puts "Run `git push` to sync changes."
  # exit
  # end
  # end
  # end

  # desc 'Initial Deploy'
  # task :initial do
  # on roles(:app) do
  # before 'deploy:restart', 'puma:start'
  # invoke 'deploy'
  # end
  # end

  # desc 'Restart application'
  # task :restart do
  # on roles(:app), in: :sequence, wait: 5 do
  # invoke 'puma:restart'
  # end
  # end

  # #after  :finishing,    :compile_assets
  # after  :finishing,    :cleanup
  # after  :finishing,    :restart

  desc 'Invoke rake task'
  task :invoke do
    on roles(:app) do
      with rails_env: :production do
        within release_path do
          execute :rake, (ENV['task']).to_s
        end
      end
    end
  end
end
