# frozen_string_literal: true

Rails.application.routes.draw do
  get 'user_group_incidents/index'
  # devise_for :users
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    unlocks: 'users/unlocks',
    confirmations: 'users/confirmations'
  }, defaults: { format: :json }, path: '', path_names: {
    sign_in: 'login', sign_out: 'logout', registration: 'signup'
  }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #

  resources :incident_categories
  resources :group_roles

  namespace :user do
    resources :incidents do
      resources :complements, only: %i[create index], controller: 'incident_complements'
    end
    post '/panic_event', to: 'incidents#create', as: :panic_event_create, defaults: { panic_event: true }

    resources :attachments

    resources :groups do
      # Security Plans
      put '/activate_security_plan', action: :set_security_plan
      put '/governance_model/centralized', action: :set_gov_centralized_person
      put '/governance_model/weighted', action: :set_gov_user_weights

      # Member ADD/REMOVE
      put '/members/:username', action: :add_member, as: :add_group_member
      delete '/members/:username', action: :remove_member, as: :remove_group_member
      resources :security_plans do
        get 'template', on: :collection
        put 'stable', on: :collection, action: :set_stable_options
      end

      resources :incidents, controller: 'group_incidents'

      resources :polls, controller: 'group_polls'
    end

    resources :group_memberships, only: %i[index update]

    resources :polls do
      post '/votes', to: 'polls#vote', on: :collection
    end
  end

  get 'zipcodes/:zip_code', to: 'zip_codes#show', as: :zip_codes
end
